export interface code {
    open: string;
    close: string;
};

export module Codes {
    export let bgBlack: code = { open: '\u001b[40m', close: '\u001b[49m' };
    export let bgBlue: code = { open: '\u001b[44m', close: '\u001b[49m' };
    export let bgCyan: code = { open: '\u001b[46m', close: '\u001b[49m' };
    export let bgGray: code = { open: '\u001b[90m', close: '\u001b[49m' };
    export let bgGreen: code = { open: '\u001b[42m', close: '\u001b[49m' };
    export let bgGrey: code = { open: '\u001b[90m', close: '\u001b[49m' };
    export let bgMagenta: code = { open: '\u001b[45m', close: '\u001b[49m' };
    export let bgRed: code = { open: '\u001b[41m', close: '\u001b[49m' };
    export let bgWhite: code = { open: '\u001b[47m', close: '\u001b[49m' };
    export let bgYellow: code = { open: '\u001b[43m', close: '\u001b[49m' };

    export let backgrounds: code[] = [
        bgBlack,
        bgBlue,
        bgCyan,
        bgGray,
        bgGreen,
        bgGrey,
        bgMagenta,
        bgRed,
        bgWhite,
        bgYellow
    ];

    export let black: code = { open: '\u001b[30m', close: '\u001b[39m' };
    export let blue: code = { open: '\u001b[34m', close: '\u001b[39m' };
    export let bold: code = { open: '\u001b[1m', close: '\u001b[22m' };
    export let cyan: code = { open: '\u001b[36m', close: '\u001b[39m' };
    export let dim: code = { open: '\u001b[2m', close: '\u001b[22m' };
    export let gray: code = { open: '\u001b[90m', close: '\u001b[39m' };
    export let green: code = { open: '\u001b[32m', close: '\u001b[39m' };
    export let grey: code = { open: '\u001b[90m', close: '\u001b[39m' };
    export let magenta: code = { open: '\u001b[35m', close: '\u001b[39m' };
    export let red: code = { open: '\u001b[31m', close: '\u001b[39m' };
    export let white: code = { open: '\u001b[37m', close: '\u001b[39m' };
    export let yellow: code = { open: '\u001b[33m', close: '\u001b[39m' };

    export let colors: code[] = [
        black,
        blue,
        cyan,
        gray,
        green,
        grey,
        magenta,
        red,
        white,
        yellow,
    ];

    export let hidden: code = { open: '\u001b[8m', close: '\u001b[28m' };
    export let inverse: code = { open: '\u001b[7m', close: '\u001b[27m' };
    export let italic: code = { open: '\u001b[3m', close: '\u001b[23m' };
    export let reset: code = { open: '\u001b[0m', close: '\u001b[0m' };
    export let strikethrough: code = { open: '\u001b[9m', close: '\u001b[29m' };
    export let underline: code = { open: '\u001b[4m', close: '\u001b[24m' };
}

export function color(s: string, color: code): string {
    return color.open + s + color.close;
}

export function bgBlack(s: string): string { return color(s, Codes.bgBlack); }
export function bgBlue(s: string): string { return color(s, Codes.bgBlue); }
export function bgCyan(s: string): string { return color(s, Codes.bgCyan); }
export function bgGray(s: string): string { return color(s, Codes.bgGray); }
export function bgGreen(s: string): string { return color(s, Codes.bgGreen); }
export function bgGrey(s: string): string { return color(s, Codes.bgGrey); }
export function bgMagenta(s: string): string { return color(s, Codes.bgMagenta); }
export function bgRed(s: string): string { return color(s, Codes.bgRed); }
export function bgWhite(s: string): string { return color(s, Codes.bgWhite); }
export function bgYellow(s: string): string { return color(s, Codes.bgYellow); }
export function black(s: string): string { return color(s, Codes.black); }
export function blue(s: string): string { return color(s, Codes.blue); }
export function bold(s: string): string { return color(s, Codes.bold); }
export function cyan(s: string): string { return color(s, Codes.cyan); }
export function dim(s: string): string { return color(s, Codes.dim); }
export function gray(s: string): string { return color(s, Codes.gray); }
export function green(s: string): string { return color(s, Codes.green); }
export function grey(s: string): string { return color(s, Codes.grey); }
export function hidden(s: string): string { return color(s, Codes.hidden); }
export function inverse(s: string): string { return color(s, Codes.inverse); }
export function italic(s: string): string { return color(s, Codes.italic); }
export function magenta(s: string): string { return color(s, Codes.magenta); }
export function red(s: string): string { return color(s, Codes.red); }
export function reset(s: string): string { return color(s, Codes.reset); }
export function strikethrough(s: string): string { return color(s, Codes.strikethrough); }
export function underline(s: string): string { return color(s, Codes.underline); }
export function white(s: string): string { return color(s, Codes.white); }
export function yellow(s: string): string { return color(s, Codes.yellow); }
