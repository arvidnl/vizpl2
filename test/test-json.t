Test JSON output:

  $ ./test.sh --json test/pipeline-1.json
  [
    {
      "label": "test/pipeline-1.json",
      "count": 1,
      "stages": [
        {
          "name": "build",
          "duration": {
            "mean": 4.666666666666667,
            "median": 4.666666666666667,
            "stddev": 0,
            "min": 4.666666666666667,
            "max": 4.666666666666667
          },
          "started_offset": {
            "mean": 123,
            "median": 123,
            "stddev": 0,
            "min": 123,
            "max": 123
          },
          "queued_duration": {
            "mean": 123,
            "median": 123,
            "stddev": 0,
            "min": 123,
            "max": 123
          },
          "status": {
            "created": 0,
            "pending": 0,
            "running": 0,
            "failed": 0,
            "success": 0,
            "canceled": 0,
            "skipped": 0,
            "manual": 0
          }
        }
      ],
      "jobs": [
        {
          "name": "job1",
          "job_ids": [
            0
          ],
          "duration": {
            "mean": 1,
            "median": 1,
            "stddev": 0,
            "min": 1,
            "max": 1
          },
          "started_offset": {
            "mean": 123,
            "median": 123,
            "stddev": 0,
            "min": 123,
            "max": 123
          },
          "queued_duration": {
            "mean": 123,
            "median": 123,
            "stddev": 0,
            "min": 123,
            "max": 123
          },
          "status": {
            "created": 0,
            "pending": 0,
            "running": 0,
            "failed": 0,
            "success": 1,
            "canceled": 0,
            "skipped": 0,
            "manual": 0
          },
          "on_critical_path": false
        },
        {
          "name": "job2",
          "job_ids": [
            1
          ],
          "duration": {
            "mean": 3,
            "median": 3,
            "stddev": 0,
            "min": 3,
            "max": 3
          },
          "started_offset": {
            "mean": 123,
            "median": 123,
            "stddev": 0,
            "min": 123,
            "max": 123
          },
          "queued_duration": {
            "mean": 123,
            "median": 123,
            "stddev": 0,
            "min": 123,
            "max": 123
          },
          "status": {
            "created": 0,
            "pending": 0,
            "running": 0,
            "failed": 1,
            "success": 0,
            "canceled": 0,
            "skipped": 0,
            "manual": 0
          },
          "on_critical_path": false
        },
        {
          "name": "job3",
          "job_ids": [
            2
          ],
          "duration": {
            "mean": 10,
            "median": 10,
            "stddev": 0,
            "min": 10,
            "max": 10
          },
          "started_offset": {
            "mean": 123,
            "median": 123,
            "stddev": 0,
            "min": 123,
            "max": 123
          },
          "queued_duration": {
            "mean": 123,
            "median": 123,
            "stddev": 0,
            "min": 123,
            "max": 123
          },
          "status": {
            "created": 0,
            "pending": 0,
            "running": 0,
            "failed": 0,
            "success": 1,
            "canceled": 0,
            "skipped": 0,
            "manual": 0
          },
          "on_critical_path": true
        }
      ],
      "pipeline": {
        "duration": {
          "mean": 10,
          "median": 10,
          "stddev": 0,
          "min": 10,
          "max": 10
        },
        "started_offset": {
          "mean": 123,
          "median": 123,
          "stddev": 0,
          "min": 123,
          "max": 123
        },
        "queued_duration": {
          "mean": 123,
          "median": 123,
          "stddev": 0,
          "min": 123,
          "max": 123
        },
        "status": {
          "created": 0,
          "pending": 0,
          "running": 0,
          "failed": 0,
          "success": 0,
          "canceled": 0,
          "skipped": 0,
          "manual": 0
        }
      }
    }
  ]
