export type project_id = {
    ns: string,
    project: string
} | number

export function project_id_tostring(p: project_id): string {
    if (p['ns']) {
        return p['ns'] + '/' + p['project'];
    } else {
        return '' + p;
    }
}

export type pipeline_ref = {
    id: number,
    project_id: project_id
}

export function pipeline_ref_tostring(ref: pipeline_ref): string {
    return project_id_tostring(ref.project_id) + '#' + ref.id;
}

export type branch_ref = {
    name: string,
    project_id: project_id
}

export function branch_ref_tostring(ref: branch_ref): string {
    return project_id_tostring(ref.project_id) + '/' + ref.name;
}

export type merge_request_ref = {
    id: number,
    project_id: project_id
}

export function merge_request_ref_tostring(ref: merge_request_ref): string {
    return project_id_tostring(ref.project_id) + '!' + ref.id;
}

function parse_ref(
    ref: string,
    obj_delimiter: string,
    default_namespace: string,
    default_project: string
): [project_id, string, string] {
    function mk_project_id(project = default_project, ns = default_namespace) {
        return {
            'ns': ns,
            'project': project
        };
    }

    let re_full = new RegExp(
        /^(?<ns>[\w0-9_\-.]+)\/(?<project>[\w0-9_\-.]+)/.source
        + obj_delimiter
        + /(?<reference>\w+)/.source
    )
    let m = ref.match(re_full);
    if (m) {
        return [mk_project_id(m.groups['project'], m.groups['ns']), m.groups['reference'], ref.substr(m[0].length)];
    }

    let re_project = new RegExp(
        /^(?<project>[\w0-9_\-.]+)/.source
        + obj_delimiter
        + /(?<reference>\w+)/.source
    );
    m = ref.match(re_project);
    if (m) {
        return [mk_project_id(m.groups['project']), m.groups['reference'], ref.substr(m[0].length)];
    }


    let re_short = new RegExp(
        /^/.source
        + obj_delimiter
        + /(?<reference>\w+)/.source
    );
    m = ref.match(re_short);
    if (m) {
        return [mk_project_id(), m.groups['reference'], ref.substr(m[0].length)];
    }

    return null;
}

export function parse_pipeline_ref(
    pipeline_ref: string,
    default_namespace: string,
    default_project: string)
    : [pipeline_ref, string] {
    // parse as a link, e.g.
    // 'https://gitlab.com/tezos/tezos/-/pipelines/1234'
    let m = pipeline_ref.match(
        /https?:\/\/gitlab\.com\/([^\/]+)\/([^\/]+)\/-\/pipelines\/(\d+)/
    );
    if (m) {
        return [{
            id: parseInt(m[3], 10),
            project_id: {
                ns: m[1],
                project: m[2],
            }
        }, ''];
    }

    // parse as a bare pipeline id, i.e. '1234'
    if (pipeline_ref.match(/^\d+$/)) {
        return [{
            'id': parseInt(pipeline_ref, 10),
            'project_id': {
                'ns': default_namespace,
                'project': default_project,
            }
        }, ''];
    }

    // parse as either:
    // - NS/PROJECT#ID (e.g. tezos/tezos#1234)
    // - PROJECT#ID (e.g. tezos#1234)
    // - #ID (e.g. #1234)
    let r = parse_ref(pipeline_ref, '#', default_namespace, default_project);
    if (r) {
        let [project_id, reference, ref_rest] = r;
        return [{
            'id': parseInt(reference, 10),
            'project_id': project_id,
        }, ref_rest];
    } else {
        return null;
    }
}


export function parse_merge_request_ref(
    merge_request_ref: string,
    default_namespace: string,
    default_project: string)
    : [merge_request_ref, string] {
    let r = parse_ref(
        merge_request_ref, '!', default_namespace, default_project
    );
    if (r) {
        let [project_id, reference, ref_rest] = r;
        return [{
            'id': parseInt(reference, 10),
            'project_id': project_id,
        }, ref_rest];
    } else {
        return null;
    }
}


export function parse_branch_ref(
    branch_ref: string,
    default_namespace: string,
    default_project: string)
    : [branch_ref, string] {
    let r = parse_ref(
        branch_ref, '/', default_namespace, default_project
    );
    if (r) {
        let [project_id, reference, ref_rest] = r;
        return [{
            'name': reference,
            'project_id': project_id,
        }, ref_rest];
    } else {
        return null;
    }
}
