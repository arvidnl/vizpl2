#!/bin/sh

# Just run the docker image with some example JSON files to detect
# obvious failures where the applications exits abnormally with error
# code != 0.

set -eu

image=${1:-}
pwd=$(pwd)

vizpl2d() {
    docker run --tty -v"$pwd":"$pwd" -w"$pwd" "$image" "$@"
}

vizpl2d "$pwd"/test/pipeline-1.json

vizpl2d "$pwd"/test/pipeline-1.json "$pwd"/test/pipeline-2.json

vizpl2d --graph "$pwd"/test/pipeline-1.json
