import { formatter } from "./formatting.js";
import { Option, some, none, is_none, is_some, of_some } from "./option.js";
import { output_type } from "./output.js";
import * as Pipelines from "./pipelines.js";

export interface Config {
    formatter: formatter;
    outputs: Set<output_type>;
    job_filter: Option<RegExp>;
    gl_access_token: Option<string>;
    sort: Pipelines.Sort;
    max_column_width: number;
}
