Test visualizing pipelines with retried jobs.

  $ ./test.sh --graph test/pipeline-retried.json
  Dataset                    Job           Time (█ = 15s, ░ = time queued, * = on critical path)
  -------------------------- ------------- ------------------------------
  test/pipeline-retried.json [pipeline]    ▏ ████████████   (0s, 3m0s)
  test/pipeline-retried.json > [build]     ▏ ████   (0s, 1m0s)
  test/pipeline-retried.json   - job1 (#1) ▏ ████   (0s, 1m0s, F, *)
  test/pipeline-retried.json   - job1 (#2) ▏     ████   (0s, 1m0s)
  test/pipeline-retried.json   - job2      ▏     ████   (0s, 1m0s, *)
  test/pipeline-retried.json   - job3      ▏         ████   (0s, 1m0s, *)
  $ ./test.sh --table test/pipeline-retried.json
  +-----------------------------------------------------------------------------+
  |                                Job durations                                |
  +-------------------------+---------------------------------------------------+
  |           Job           | test/pipeline-retried.json (n=1): Duration (mean) |
  +-------------------------+---------------------------------------------------+
  | [pipeline]              |                                              3m0s |
  | > [build]               |                                              1m0s |
  |   - job1 (#1)           |                                              1m0s |
  |   - job1 (#2)           |                                              1m0s |
  |   - job2                |                                              1m0s |
  |   - job3                |                                              1m0s |
  |                         |                                                   |
  | Sequential time (mean): |                                              4m0s |
  +-------------------------+---------------------------------------------------+
