'use strict';

import { Context } from "./context.js";
import * as Statistics from "./statistics.js";
import { Stats } from "./statistics.js";
import * as Vzdate from "./vzdate.js";

// import { error } from "./error.js";

interface JobRaw {
    id: number;
    name: string;
    duration: number | null;
    queued_duration: number | null;
    status: string;
    pipeline: {
        id: number
    },
    stage: string;
    allow_failure: boolean;
    started_at: string | null;
    finished_at: string | null;
}

export type raw_pipeline = JobRaw[]

interface BridgeRaw {
    downstream_pipeline: {
        id: number;
    }
}

export type raw_bridges = BridgeRaw[]

// we preprocess raw pipelines to add some extra informatoin
interface JobAnnotations {
    // the offset of the starting date w.r.t. earlier job in pipeline
    started_at_offset: number,
    // true if we infer that this job is on the pipeline's critical path
    on_critical_path: boolean
}

// jobs as returned by gitlab api after preprocessing
interface JobAnnotated extends JobRaw, JobAnnotations {
}

// an abstract job is either a normal job or a pseudo job (stages,
// all). it is always annotated
interface AbstractJob extends JobAnnotations {
    id: number;
    name: string;
    stage: string;
    duration: number;
    queued_duration: number;
    status: string;
    pipeline: {
        id: number
    }
}

export enum Sort {
    Duration = 1,
    Started_at,
}

export type pipeline = JobAnnotated[]

export function annotate_pipeline(raw_pl: raw_pipeline, aggregate: boolean = false): pipeline {
    let min_start_date = Statistics.min(
        raw_pl
            .filter((raw_job) => raw_job.started_at != null)
            .map((raw_job) =>
                Vzdate.date_str_to_epoch(raw_job.started_at) -
                (raw_job.queued_duration !== null && raw_job.queued_duration !== undefined ?
                    raw_job.queued_duration : 0)
            )
    );

    let pl = raw_pl.map((raw_job) => {
        let started_at_offset =
            raw_job.started_at != null
                ? Vzdate.date_str_to_epoch(raw_job.started_at) - min_start_date
                : null;
        return {
            ...raw_job,
            started_at_offset: started_at_offset,
            stage: aggregate ? "" : raw_job.stage,
            on_critical_path: false,
        }
    });

    // Find retried jobs and label them
    let jobs_by_name: { [index: string]: number[] } = {};
    for (let job of pl) {
        if (job.name in jobs_by_name) {
            jobs_by_name[job.name].push(job.id)
        } else {
            jobs_by_name[job.name] = [job.id]
        }
    }
    for (let job_name in jobs_by_name) {
        jobs_by_name[job_name] = jobs_by_name[job_name].sort((job_id1, job_id2) => job_id1 - job_id2)
    }
    pl = pl.map((job) => {
        if (jobs_by_name[job.name].length > 1) {
            let index = jobs_by_name[job.name].indexOf(job.id);
            job.name = job.name + " (#" + (index + 1) + ")";
            return job;
        } else {
            return job;
        }
    });

    // To find the critical path, we find the immediate predecessor of
    // each job J. The immediate predecessor is the job that
    // terminated most recently before the start of the job J.
    let jobs_by_started_at: [number, JobAnnotated][] = pl.map((job) => [job.started_at_offset - job.queued_duration, job]);
    jobs_by_started_at.sort((job1, job2) => job1[0] - job2[0]);

    let jobs_by_finished_at: [number, JobAnnotated][] = pl
        .filter((job) => job.finished_at !== null)
        .map((job) => [Vzdate.date_str_to_epoch(job.finished_at) - min_start_date, job]);
    jobs_by_finished_at.sort((job1, job2) => job1[0] - job2[0]);
    // If no jobs have finished, then there is no critical path.
    if (jobs_by_finished_at.length > 0) {
        const last_job = jobs_by_finished_at[jobs_by_finished_at.length - 1][1];

        // jobs_predecessor[job1] = job2 if job2 is the predecessor of job1
        let jobs_predecessor: { [index: number]: JobAnnotated } = {};

        // [finished_at_index] will point to the possible job in
        // jobs_by_finished_at that might've finished earlier than current
        // job in the loop below.
        let finished_at_index = 0;
        for (const [job_started_at, job] of jobs_by_started_at) {
            // if 'finished_at_index == jobs_by_finished_at.length' then
            // there are no jobs that finished before the current job.
            if (finished_at_index < jobs_by_finished_at.length) {
                // last, if non-null, is the job with the latest 'finished_at' that is
                // less than or equal to job_started_at
                let last: JobAnnotated = null;
                while (finished_at_index < jobs_by_finished_at.length
                    && jobs_by_finished_at[finished_at_index][0] <= job_started_at) {
                    if (jobs_by_finished_at[finished_at_index][0] != job.id) {
                        last = jobs_by_finished_at[finished_at_index][1];
                    }
                    finished_at_index++;
                }

                finished_at_index = finished_at_index > 0 ? finished_at_index - 1 : 0;

                if (last) {
                    jobs_predecessor[job.id] = last;
                }
            }
        }

        // Now, the critical path is the last job and its sequence of
        // predecessors
        let finished_last: JobAnnotated = last_job;
        while (finished_last !== undefined) {
            finished_last.on_critical_path = true;
            finished_last = jobs_predecessor[finished_last.id];
        }
    }

    return pl;
}

// Merged jobs and datasets

export interface Dataset {
    label: string;
    pipelines: pipeline[];
}

interface StatusStats {
    canceled: number;
    created: number;
    failed: number;
    manual: number;
    pending: number;
    running: number;
    skipped: number;
    success: number;
}

// Represents either a set of merged concrete jobs or a set of merged pseudo jobs
export interface MergedJob {
    name: string,
    started_offset_stats: Stats;
    duration_stats: Stats;
    queued_duration_stats: Stats;
    status_stats: StatusStats;
    jobs: AbstractJob[];

    // pseudo-jobs do not have the following properties
    stage: string;

    // on_critical_path is the OR of the on_critical_path of the underlying jobs
    on_critical_path: boolean;
}

export interface MergedDataset {
    label: string;
    // number of merged sets
    count: number;
    merged_jobs: { [index: string]: MergedJob };
    merged_stages: { [index: string]: MergedJob };
    all: MergedJob;
}

function merge_jobs(
    ctxt: Context,
    ds_length: number,
    job_name: string,
    jobs: AbstractJob[]
): MergedJob {
    if (jobs.length == 0) {
        ctxt.error("cannot merge 0 jobs");
    }

    let stage = jobs[0].stage;
    let other_stages = jobs.filter((job) => job.stage != stage)
    if (other_stages.length != 0) {
        ctxt.error("expected jobs to all contains jobs of stage " + stage);
    }

    let durations = jobs
        .filter((job) => job.duration !== null && job.duration !== undefined)
        .map((job) => job.duration);

    let queued_durations = jobs
        .filter((job) => job.queued_duration !== null && job.queued_duration !== undefined)
        .map((job) => job.queued_duration);

    if (durations.length == 0) {
        // console.warn("Found no durations for job: " + job_name)
        // continue;
    }

    let started_offsets = jobs.map((job) => job.started_at_offset);

    let on_critical_path = jobs.reduce((on_critical_path, job) => job.on_critical_path || on_critical_path, false);

    return {
        name: job_name,
        stage,
        started_offset_stats: Statistics.stats(started_offsets),
        duration_stats: Statistics.stats(durations),
        queued_duration_stats: Statistics.stats(queued_durations),
        status_stats: {
            created: Statistics.count(jobs, (job) => job.status == "created"),
            pending: Statistics.count(jobs, (job) => job.status == "pending"),
            running: Statistics.count(jobs, (job) => job.status == "running"),
            failed: Statistics.count(jobs, (job) => job.status == "failed"),
            success: Statistics.count(jobs, (job) => job.status == "success"),
            canceled: Statistics.count(jobs, (job) => job.status == "canceled"),
            skipped: Statistics.count(jobs, (job) => job.status == "skipped"),
            manual: Statistics.count(jobs, (job) => job.status == "manual")
        },
        jobs,
        on_critical_path
    };
}

function merge_jobs_hash(
    ctxt: Context,
    ds_length: number,
    jobs_hash: { [index: string]: AbstractJob[] }
): { [index: string]: MergedJob } {

    let merged_jobs: { [index: string]: MergedJob } = {}
    for (const job_name of Object.keys(jobs_hash)) {
        merged_jobs[job_name] = merge_jobs(ctxt, ds_length, job_name, jobs_hash[job_name])
    }
    return merged_jobs;
}


function status_of_jobs(jobs: JobAnnotated[]): string {
    let failed_jobs = jobs.filter((job) => job.status != "success" && !job.allow_failure)
    // if (failed_jobs.length > 0) {
    // console.warn("considering stage " + stage + " in pipeline " + jobs[0].pipeline.id + "as failure due to jobs:")
    // failed_jobs.forEach((job) => console.log(job))
    // }
    return (failed_jobs.length > 0 ? "failure" : "success");
}

export function merge_dataset(
    ctxt: Context,
    ds: Dataset): MergedDataset {
    let jobs_by_name: { [index: string]: AbstractJob[] } = {}
    ds.pipelines.forEach((pipeline) => {
        pipeline.forEach((job) => {
            if (job.name in jobs_by_name) {
                jobs_by_name[job.name].push(job)
            } else {
                jobs_by_name[job.name] = [job]
            }
        })
    });

    let merged_jobs = merge_jobs_hash(ctxt, ds.pipelines.length, jobs_by_name);

    let stages_by_name: { [index: string]: AbstractJob[] } = {}
    ds.pipelines.forEach((pipeline) => {
        let pipeline_id = pipeline[0].pipeline.id;

        let stage_names: Set<string> = new Set(pipeline.map((job) => job.stage));
        let stages = [];
        for (let stage of stage_names) {
            let jobs = pipeline.filter((job) => job.stage == stage);
            if (jobs.length == 0) {
                // console.warn("Found no jobs in stage: " + stage + ", of pipeline" + jobs[0].pipeline.id)
                continue;
            }

            let stage_status = status_of_jobs(jobs);

            let jobs_with_duration =
                jobs.filter((job) => job.duration !== null && job.duration !== undefined)
            let durations = jobs_with_duration.map((job) => job.duration);

            let jobs_with_queued_duration =
                jobs.filter((job) => job.queued_duration !== null && job.queued_duration !== undefined)
            let queued_durations = jobs_with_queued_duration.map((job) => job.queued_duration);

            if (durations.length == 0) {
                // console.warn("Found no durations for jobs in stage: " + stage + " in pipeline " + jobs[0].pipeline.id + ", skipping")
                continue;
            }

            let min_started_at = Statistics.min(
                jobs_with_duration.map((job) => job.started_at_offset)
            );

            let stage_job: AbstractJob = {
                id: 0, // TODO: hack
                name: stage,
                stage: "_stages", // TODO: hack
                started_at_offset: min_started_at,
                duration: Statistics.mean(durations),
                queued_duration: Statistics.mean(queued_durations),
                status: stage_status,
                pipeline: { id: pipeline_id },
                on_critical_path: false
            }
            if (stage in stages_by_name) {
                stages_by_name[stage].push(stage_job);
            } else {
                stages_by_name[stage] = [stage_job];
            }
        }
        return stages;
    });
    let merged_stages = merge_jobs_hash(ctxt, ds.pipelines.length, stages_by_name);

    let all_jobs: AbstractJob[] =
        ds.pipelines.map((pipeline) => {
            let pipeline_id = pipeline[0].pipeline.id;
            let started_jobs = pipeline.filter((job) => job.started_at != null)
            let started_at_offset = Statistics.min(started_jobs.map((job) => job.started_at_offset));

            let start_time = Statistics.min(started_jobs.map((job) => Vzdate.date_str_to_epoch(job.started_at)));
            let end_time = Statistics.max(started_jobs.map((job) => Vzdate.date_str_to_epoch(job.finished_at)));
            let duration = end_time - start_time;

            let all_status = status_of_jobs(pipeline);
            return {
                id: -1, // TODO: hack
                name: "all",
                stage: "_all",
                started_at_offset: started_at_offset,
                duration: duration,
                // TODO: does this make any sense?
                queued_duration: 0,
                status: all_status,
                pipeline: { id: pipeline_id },
                on_critical_path: false
            }
        });
    let merged_all = merge_jobs(ctxt, ds.pipelines.length, "all", all_jobs);

    return {
        label: ds.label,
        count: ds.pipelines.length,
        merged_jobs: merged_jobs,
        merged_stages: merged_stages,
        all: merged_all
    }
}

type job_matrix = (MergedJob | undefined)[][]

function join_merged_dataset_jobs(stage_name: string, baseline: MergedDataset, others: MergedDataset[], sort: Sort): job_matrix {
    let baseline_stage_jobs = Object
        .values(baseline.merged_jobs)
        .filter((job) => job.stage == stage_name);
    if (sort == Sort.Duration)
        baseline_stage_jobs.sort((j1, j2) => j2.duration_stats.mean - j1.duration_stats.mean)
    else if (sort == Sort.Started_at)
        baseline_stage_jobs.sort((j1, j2) => j1.started_offset_stats.mean - j2.started_offset_stats.mean)

    // for each job, find corresponding in others and add it or null
    let stage_job_rows: job_matrix =
        baseline_stage_jobs.map((baseline_stage_job) =>
            [baseline_stage_job].concat(others.map((ds) => {
                let job = ds.merged_jobs[baseline_stage_job.name];
                if (job != undefined && job.stage == stage_name) {
                    delete ds.merged_jobs[baseline_stage_job.name];
                    return job;
                } else {
                    return undefined;
                }
            })));

    baseline_stage_jobs.forEach((job) => delete baseline.merged_jobs[job.name]);

    if (others.length != 0) {
        let others_stage_jobs_rows: job_matrix = join_merged_dataset_jobs(
            stage_name, others[0], others.slice(1), sort
        );
        others_stage_jobs_rows.forEach((row) => row.unshift(undefined));
        stage_job_rows = stage_job_rows.concat(others_stage_jobs_rows)
    }

    return stage_job_rows;
}

function join_merged_dataset_stages(baseline: MergedDataset, others: MergedDataset[], sort: Sort): job_matrix {
    let job_matrix: job_matrix = [];

    let stages_sorted: MergedJob[] = Object.values(baseline.merged_stages);
    stages_sorted = stages_sorted.sort(
        (s1, s2) => s1.started_offset_stats.median - s2.started_offset_stats.median
    )

    for (let stage_job of stages_sorted) {
        let stage_name = stage_job.name;

        let baseline_stage_jobs = Object
            .values(baseline.merged_jobs)
            .filter((job) => job.stage == stage_name);
        if (baseline_stage_jobs.length > 0) {
            job_matrix.push([stage_job].concat(others.map((ds) => ds.merged_stages[stage_name])));
            job_matrix = job_matrix.concat(join_merged_dataset_jobs(stage_name, baseline, others, sort));
        }
    }

    if (others.length != 0) {
        let sub_job_matrix: job_matrix = join_merged_dataset_stages(
            others[0], others.slice(1), sort
        );
        sub_job_matrix.forEach((row) => row.unshift(undefined));
        job_matrix = job_matrix.concat(sub_job_matrix);
    }

    return job_matrix;
}

export function join_merged_dataset(baseline: MergedDataset, others: MergedDataset[], sort: Sort): job_matrix {
    let job_matrix: job_matrix = [];

    // Add a line for the full pipeline
    job_matrix.push([baseline.all].concat(others.map((ds) => ds.all)));
    // Add each stage
    job_matrix = job_matrix.concat(join_merged_dataset_stages(baseline, others, sort));

    // Sanity check
    let row_lengths = job_matrix.map((row) => row.length);
    if ((new Set(row_lengths)).size != 1) {
        console.error("Row lengths are funky", row_lengths)
    }

    return job_matrix;
}

