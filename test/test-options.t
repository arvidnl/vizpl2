
  $ ./test.sh --help
  Usage: vizpl2 [OPTIONS] [PIPELINE_REFERENCES]
  
  Visualize the executions of the GitLab CI pipelines PIPELINE_REFERENCES.
  Example: vizpl2 --graph arvidnl/vizpl2#725494153
  
  PIPELINE REFERENCES
  
    Remote pipelines are fetched through GitLab's API and are referred to
    either by a pipeline reference 'NAMESPACE/PROJECT#PIPELINE_ID', or by an
    URL 'https://gitlab.com/NAMESPACE/PROJECT/-/pipelines/PIPELINE_ID'.
    Alternatively, a path to a JSON file containing results of GitLab's
    'List pipeline jobs' REST API can be given 'local_file.json' (see
    https://docs.gitlab.com/ee/api/jobs.html#list-pipeline-jobs).
  
    Multiple references can be supplied. By default, each reference form
    a single dataset containing a single pipeline, labelled by the reference.
  
    A sequence of references can be merged to a single dataset when
    terminated by a '--label LABEL' flag.  In this case, if no terminating
    '--label' flag is given, all trailing references are merged into an
    anonymous dataset.
  
  EXAMPLES
  
    For instance:
  
      vizpl2 pipeline-123123.json
  
    visualizes the result in 'pipeline-123123.json'.
  
      vizpl2 ns/proj#111 ns/proj#222
  
    gives two datasets:
      - a dataset 'ns/proj#111' containing #111,
      - a dataset 'ns/proj#222' containing #222,
  
      vizpl2 ns/proj#111 ns/proj#222 --label ds1 ns/proj#333 --label ds2 ns/proj#444
  
    gives the following datasets:
      - a dataset 'ds1' containing pipelines #111 and #222,
      - a dataset 'ds2' containing pipeline #333,
      - an anonymous dataset containing pipeline #444.
  
  
  OPTIONS
  
    --humanize                  Format durations in HH:MM:SS format (default).
    --seconds                   Format durations as seconds.
    --table                     Present results as a table.
    --graph                     Present results as a graph.
    --json                      Present results as json.
    --job-filter <REGEXP>       Filter job by regexp. Example: '--job-filter "build_\*"'.
    --merge-stages              Merge pipelines stages.
    --sort-by-started-at        Sort jobs by start time.
    --sort-by-duration          Sort jobs by duration.
    --max-column-width <WIDTH>  Maximum column width.
    --no-color                  Disable color in output.
    --color                     Enable color in output.
    --no-include-retried        Do not include retried jobs.
    --help                      Show this help.
    --version                   Show version.
  [1]

Options

  $ ./test.sh test/pipeline-1.json
  +-----------------------------------------------------------------------+
  |                             Job durations                             |
  +-------------------------+---------------------------------------------+
  |           Job           | test/pipeline-1.json (n=1): Duration (mean) |
  +-------------------------+---------------------------------------------+
  | [pipeline]              |                                         10s |
  | > [build]               |                                       4.67s |
  |   - job3                |                                         10s |
  |   - job2                |                                          3s |
  |   - job1                |                                          1s |
  |                         |                                             |
  | Sequential time (mean): |                                         14s |
  +-------------------------+---------------------------------------------+
  $ ./test.sh --humanize test/pipeline-1.json
  +-----------------------------------------------------------------------+
  |                             Job durations                             |
  +-------------------------+---------------------------------------------+
  |           Job           | test/pipeline-1.json (n=1): Duration (mean) |
  +-------------------------+---------------------------------------------+
  | [pipeline]              |                                         10s |
  | > [build]               |                                       4.67s |
  |   - job3                |                                         10s |
  |   - job2                |                                          3s |
  |   - job1                |                                          1s |
  |                         |                                             |
  | Sequential time (mean): |                                         14s |
  +-------------------------+---------------------------------------------+
  $ ./test.sh --job-filter job3 test/pipeline-1.json
  +-----------------------------------------------------------------------+
  |                             Job durations                             |
  +-------------------------+---------------------------------------------+
  |           Job           | test/pipeline-1.json (n=1): Duration (mean) |
  +-------------------------+---------------------------------------------+
  |   - job3                |                                         10s |
  |                         |                                             |
  | Sequential time (mean): |                                         10s |
  +-------------------------+---------------------------------------------+
  $ ./test.sh --merge-stages test/pipeline-1.json
  +-----------------------------------------------------------------------+
  |                             Job durations                             |
  +-------------------------+---------------------------------------------+
  |           Job           | test/pipeline-1.json (n=1): Duration (mean) |
  +-------------------------+---------------------------------------------+
  | [pipeline]              |                                         10s |
  | > []                    |                                       4.67s |
  |   - job3                |                                         10s |
  |   - job2                |                                          3s |
  |   - job1                |                                          1s |
  |                         |                                             |
  | Sequential time (mean): |                                         14s |
  +-------------------------+---------------------------------------------+
  $ ./test.sh --sort-by-started-at test/pipeline-1.json
  +-----------------------------------------------------------------------+
  |                             Job durations                             |
  +-------------------------+---------------------------------------------+
  |           Job           | test/pipeline-1.json (n=1): Duration (mean) |
  +-------------------------+---------------------------------------------+
  | [pipeline]              |                                         10s |
  | > [build]               |                                       4.67s |
  |   - job1                |                                          1s |
  |   - job2                |                                          3s |
  |   - job3                |                                         10s |
  |                         |                                             |
  | Sequential time (mean): |                                         14s |
  +-------------------------+---------------------------------------------+
  $ ./test.sh --sort-by-duration test/pipeline-1.json
  +-----------------------------------------------------------------------+
  |                             Job durations                             |
  +-------------------------+---------------------------------------------+
  |           Job           | test/pipeline-1.json (n=1): Duration (mean) |
  +-------------------------+---------------------------------------------+
  | [pipeline]              |                                         10s |
  | > [build]               |                                       4.67s |
  |   - job3                |                                         10s |
  |   - job2                |                                          3s |
  |   - job1                |                                          1s |
  |                         |                                             |
  | Sequential time (mean): |                                         14s |
  +-------------------------+---------------------------------------------+
  $ ./test.sh --max-column-width 10 test/pipeline-1.json
  +-----------------------------------------------------------------------+
  |                             Job durations                             |
  +-------------------------+---------------------------------------------+
  |           Job           | test/pipeline-1.json (n=1): Duration (mean) |
  +-------------------------+---------------------------------------------+
  | [pipeline]              |                                         10s |
  | > [build]               |                                       4.67s |
  |   - job3                |                                         10s |
  |   - job2                |                                          3s |
  |   - job1                |                                          1s |
  |                         |                                             |
  | Sequential time (mean): |                                         14s |
  +-------------------------+---------------------------------------------+
