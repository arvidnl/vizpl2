
// import { AsciiTable3, AlignmentEnum } from 'ascii-table3';
import * as AT from 'ascii-table3';

import { MergedDataset, MergedJob, join_merged_dataset } from "./pipelines.js";

import { Stats, min, max } from "./statistics.js";

import { Config } from "./datafetcher.js";

import * as Opt from "./option.js";

import * as formatting from "./formatting.js";

import * as AsciiGraph from "./asciigraph.js";

import * as AnsiColors from "./ansicolors.js";

import { pad_right } from "./string.js";

export enum output_type {
    Table,
    Graph,
    Json
}

// enum tabulation_sort { ByStage, ByStageAndDuration }

export function output(config: Config, baseline: MergedDataset, others: MergedDataset[]) {
    config.outputs.forEach((output) => {
        if (output === output_type.Table) {
            tabulate_results(config, baseline, others);
        } else if (output === output_type.Graph) {
            graph_results(config, baseline, others);
        } else if (output === output_type.Json) {
            json_results(config, baseline, others)
        }
    });
}

function job_to_statuses(job: MergedJob, color: boolean): string {
    let status: string = "";
    // Decorate non-pseudo jobs with status stats
    if (job.stage != "_all" && job.stage != "_stages") {
        let stats = job.status_stats;
        if (job.jobs.length == 1) {
            if (stats.canceled == 1) { status = "C"; }
            else if (stats.created == 1) { status = "CR"; }
            else if (stats.failed == 1) {
                status = "F";
                if (color) {
                    status = AnsiColors.color(status, AnsiColors.Codes.red);
                }
            }
            else if (stats.manual == 1) { status = "M"; }
            else if (stats.pending == 1) { status = "P"; }
            else if (stats.running == 1) { status = "R"; }
            else if (stats.skipped == 1) { status = "S"; }
            else if (stats.success == 1) { status = ""; }
            else { status = "?"; }
            if (status != "") {
                status = `${status}`;
            }
        } else {
            let statuses: string[] = [];
            if (stats.canceled > 0) { statuses.push(`${stats.canceled}C`); }
            else if (stats.created > 0) { statuses.push(`${stats.created}CR`); }
            else if (stats.failed > 0) { statuses.push(`${stats.failed}F`); }
            else if (stats.manual > 0) { statuses.push(`${stats.manual}M`); }
            else if (stats.pending > 0) { statuses.push(`${stats.pending}P`); }
            else if (stats.running > 0) { statuses.push(`${stats.running}R`); }
            else if (stats.skipped > 0) { statuses.push(`${stats.skipped}S`); }
            status = statuses.join("/");
        }
    }

    return status;
}

function job_to_row_label(job: MergedJob): string {
    let name: string;
    if (job.stage == "_all") {
        name = "[pipeline]";
    } else if (job.stage == "_stages") {
        name = `> [${job.name}]`;
    } else {
        name = `  - ${job.name}`;
    }
    return name;
}

function row_baseline(jobs: MergedJob[]): [number, MergedJob] {
    // select the first defined job in the row as the baseline
    for (let [job_idx, job] of jobs.entries()) {
        if (job != undefined) {
            return [job_idx, job];
        }
    }

}

function apply_job_filter(job_filter: Opt.Option<RegExp>, jobs_matrix: MergedJob[][]): MergedJob[][] {
    return Opt.fold(
        job_filter,
        job_filter => jobs_matrix.filter(
            jobs => {
                let [_, baseline_job] = row_baseline(jobs);
                return !!baseline_job.name.match(job_filter);
            }
        ),
        jobs_matrix
    );
}

function string_len_max(ss: string[]): number {
    return max(ss.map((s) => s.length));
}

let next_color: number = 0;
let color_of_stage: { [index: string]: AnsiColors.code } = {};
function get_color_of_stage(job: MergedJob): AnsiColors.code {
    let stage: string;
    if (job.stage == "_all") {
        stage = "pipeline";
    } else if (job.stage == "_stages") {
        stage = job.name;
    } else {
        stage = job.stage;
    }

    let graph_palette: AnsiColors.code[] = [
        AnsiColors.Codes.blue,
        AnsiColors.Codes.white,
        AnsiColors.Codes.cyan,
        AnsiColors.Codes.gray,
        AnsiColors.Codes.magenta,
        AnsiColors.Codes.green,
        AnsiColors.Codes.red,
        // used for queued
        // AnsiColors.Codes.yellow,
    ];

    if (!(stage in color_of_stage)) {
        color_of_stage[stage] = graph_palette[next_color];
        next_color = ((next_color + 1) % graph_palette.length);
    }
    return color_of_stage[stage];
}


export function graph_results(config: Config, baseline: MergedDataset, others: MergedDataset[]) {

    let jobs_matrix = join_merged_dataset(baseline, others, config.sort);
    jobs_matrix = apply_job_filter(config.job_filter, jobs_matrix);

    let max_width = config.max_column_width;

    let max_label_width = string_len_max(
        ([baseline].concat(others)).map((ds) => ds.label).concat(["Dataset"])
    );

    let max_job_name_width = string_len_max(
        jobs_matrix.flat().map((job) => job ? job_to_row_label(job) : '').concat(["Job"])
    );

    let bar_max = max_width
        - (1  // lhs
            + max_label_width
            + 3 // padding
            + max_job_name_width
            + 3 // padding
            + 12 // times
            + 10); // rhs
    // Hack to ensure that resolution does not become negative
    bar_max = Math.max(bar_max, 1);

    let job_times =
        jobs_matrix.flat().map((job) => {
            let finish_time =
                job.started_offset_stats.mean +
                job.duration_stats.mean;
            return isNaN(finish_time) ? 0 : finish_time;
        });
    let max_finish_time = max(
        job_times
    );

    let resres = 5;
    let resolution = resres * Math.ceil(max_finish_time / bar_max / resres);

    let queued_color = AnsiColors.Codes.yellow;
    let duration_char = AsciiGraph.block;
    let queued_char = '░';

    let ex_duration: string, ex_queued_duration: string, ex_critical: string;
    if (config.color) {
        ex_duration = AsciiGraph.block;
        ex_queued_duration = AnsiColors.color(AsciiGraph.block, queued_color);
        ex_critical = AnsiColors.bold('*');
    } else {
        ex_duration = duration_char;
        ex_queued_duration = queued_char;
        ex_critical = '*';
    }

    let headings: string[] = [
        pad_right("Dataset", max_label_width),
        pad_right("Job", max_job_name_width),
        `Time (${ex_duration} = ${resolution}s, ${ex_queued_duration} = time queued, ${ex_critical} = on critical path)`
    ];

    let lines: string[] = [
        '-'.repeat(max_label_width),
        '-'.repeat(max_job_name_width),
        '------------------------------'
    ];

    let rows: string[][] =
        jobs_matrix.flatMap((jobs) => {
            let rows: string[][] = [];

            let baseline_job: MergedJob;
            // let baseline_idx: number;
            for (let [_job_idx, job] of jobs.entries()) {
                if (job != undefined) {
                    baseline_job = job;
                    // baseline_idx = job_idx;
                    break;
                }
            }

            // TODO: better handling of missing jobs
            let nan =
                isNaN(baseline_job.started_offset_stats.mean) ||
                isNaN(baseline_job.duration_stats.mean);

            let bar = '';
            if (!nan) {
                let color = get_color_of_stage(baseline_job);
                let started_at = Math.floor(baseline_job.started_offset_stats.mean);
                let queued_duration = Math.floor(baseline_job.queued_duration_stats.mean);
                let duration = Math.floor(baseline_job.duration_stats.mean);

                let duration_bar = '';
                let queued_duration_bar = '';
                if (!isNaN(queued_duration)) {
                    let queue_started_at = max([started_at - queued_duration, 0]);

                    // add and offset to make queue time line up to
                    // the right border or a column, job time time to
                    // the left
                    let lineup_offset = resolution - ((queue_started_at + queued_duration) % resolution);

                    if (config.color) {
                        queued_duration_bar = AsciiGraph.block_bar(resolution, lineup_offset + queue_started_at, queued_duration, Opt.some(queued_color))[0];
                        duration_bar = AsciiGraph.block_bar(
                            resolution,
                            0, // TODO: hack
                            duration,
                            Opt.some(color)
                        )[0];
                    } else {
                        queued_duration_bar = AsciiGraph.char_bar(queued_char, resolution, lineup_offset + queue_started_at, queued_duration)[0];
                        duration_bar = AsciiGraph.char_bar(
                            duration_char,
                            resolution,
                            0, // TODO: hack
                            duration
                        )[0];
                    }
                } else {
                    if (config.color) {
                        duration_bar = AsciiGraph.block_bar(
                            resolution,
                            started_at,
                            duration,
                            Opt.some(color)
                        )[0];
                    } else {
                        duration_bar = AsciiGraph.char_bar(
                            duration_char,
                            resolution,
                            started_at,
                            duration
                        )[0];
                    }
                }

                // Decorate the bar with queued_duration, duration and failure stats
                let status = job_to_statuses(baseline_job, config.color);
                let bar_trailer: string = '(';
                if (!isNaN(queued_duration)) {
                    bar_trailer += config.formatter(queued_duration, Opt.none()) + ', ';
                }
                bar_trailer += config.formatter(duration, Opt.none());
                if (status != '') {
                    bar_trailer += ", " + status;
                }
                if (baseline_job.on_critical_path) {
                    bar_trailer += ", *";
                }
                bar_trailer += ')';
                if (config.color && baseline_job.on_critical_path) {
                    bar_trailer = AnsiColors.bold(bar_trailer);
                }

                bar += queued_duration_bar
                    + duration_bar
                    + '   ' + bar_trailer;
            }

            // TODO: add other jobs
            rows.push([
                pad_right(baseline.label, max_label_width),
                pad_right(job_to_row_label(baseline_job), max_job_name_width),
                '▏' + bar
            ])

            return rows;

        });

    let table: string[] = [headings, lines].concat(rows).map((r) => r.join(' '));

    console.log(table.join("\n"));
}


export function tabulate_results(config: Config, baseline: MergedDataset, others: MergedDataset[]) {
    // present results thus:

    // let _config = {
    //     // sort: tabulation_sort.ByStage,
    //     humanize_durations: true,
    //     compare_baseline: true,
    //     show_stats: true,
    //     show_success: true,
    // };

    // join_merged_dataset destructively modifies the baseline and the other datasets.
    // for this reason, we calculate the totals before creating the jobs_matrix.


    // configure headings
    let all_stats = ["mean", "median", "stddev", "min", "max"];
    let stats_headings = (measure: string, fields = all_stats) =>
        fields.map((stat) => `${measure} (${stat})`)
    function stats_values(stat: Stats, fields = all_stats): number[] {
        return fields.map((field) => stat[field])
    }

    let include_stats = ['mean'];
    let columns_per_ds = [
        {
            headings:
                [
                    stats_headings('Duration', include_stats),
                    // stats_headings('Queued duration', include_stats)
                ].flat(),
            values: ((job: MergedJob) =>
                [
                    stats_values(job.duration_stats, include_stats),
                    // stats_values(job.queued_duration_stats, include_stats)
                ].flat()
            )
        },
        // {headings: stats_headings('End offset', ['mean']), values: ((job: MergedJob) => stats_values(job.started_offset_stats, ['mean']).map(d2))}
        // ['Start offset', ((job: MergedJob) => job.started_offset_stats), ['mean']],
    ];


    let headings = ['Job'];
    let datasets = [baseline].concat(others);
    headings = headings.concat(
        datasets.flatMap(
            (ds) => columns_per_ds.flatMap(
                (col, i) => col.headings.map((h) =>
                    i == 0 ? `${ds.label} (n=${ds.count}): ${h}` : h))));
    let nb_headings_per_dataset = columns_per_ds.reduce(
        (nb_headings, column_group) => column_group.headings.length + nb_headings,
        0
    );

    // Calculate the bottom totals row. Must be done before
    // jobs_matrix is created below. TODO: generalize to include
    // totals for other columns.
    let sequential_time_mean_row = ['Sequential time (mean):'];
    let sequential_time_mean_baseline: Opt.Option<number> = Opt.none();
    let filler_columns = new Array(nb_headings_per_dataset - 1).fill('');
    [baseline].concat(others).forEach((dataset, dataset_index) => {
        let sequential_time = 0;
        for (const job of Object.values(dataset.merged_jobs)) {
            if (Opt.fold(config.job_filter, job_filter => !job.name.match(job_filter), false)) {
                continue;
            }
            // job.duration_stats.mean is NaN if the job had no
            // durations, i.e. all executions in the merged job had
            // status manual.
            if (isNaN(job.duration_stats.mean)) {
                continue;
            }
            sequential_time += job.duration_stats.mean;
        }
        sequential_time_mean_row.push(config.formatter(sequential_time, sequential_time_mean_baseline));
        if (dataset_index == 0) {
            sequential_time_mean_baseline = Opt.some(sequential_time);
        }
        sequential_time_mean_row = sequential_time_mean_row.concat(filler_columns);
    });


    let jobs_matrix = join_merged_dataset(baseline, others, config.sort);
    jobs_matrix = apply_job_filter(config.job_filter, jobs_matrix);

    let table = new AT.AsciiTable3('Job durations');
    table.setHeading(...headings);
    headings.slice(1).forEach((_, i) => table.setAlign(i + 2, AT.AlignmentEnum.RIGHT));
    table.addRowMatrix(jobs_matrix.map((jobs) => {
        let row: (string | number)[] = [];

        // select the first defined job in the row as the baseline
        let [baseline_idx, baseline_job] = row_baseline(jobs);

        // apply job filter
        if (!Opt.fold(config.job_filter, job_filter => !!baseline_job.name.match(job_filter), true)) {
            return [];
        }

        row.push(job_to_row_label(baseline_job));

        let baseline_values: number[][] = [];
        for (let [job_idx, job] of jobs.entries()) {
            for (let [col_idx, col] of columns_per_ds.entries()) {
                let vals: (string | number)[];
                if (job != undefined && job_idx == baseline_idx) {
                    let values_raw = col.values(job);
                    baseline_values[col_idx] = values_raw;
                    vals = values_raw.map((v) => {
                        return config.formatter(v, Opt.none());
                    });
                } else if (job != undefined) {
                    vals = col.values(job).map((v, i) => {
                        return config.formatter(v, Opt.some(baseline_values[col_idx][i]));
                    });
                } else {
                    vals = col.headings.fill("n/a");
                }
                row = row.concat(vals);
            }
        }

        return row;
    }));

    // Add sequential time
    let empty_row = new Array(1 + headings.length).fill('');
    table.addRow(...empty_row);
    table.addRow(...sequential_time_mean_row);

    console.log(table.toString().trim());
}

export function json_results(_config: Config, baseline: MergedDataset, others: MergedDataset[]) {
    let result = [];
    for (const dataset of [baseline].concat(others)) {
        let dataset_json = {
            label: dataset.label,
            count: dataset.count,
            stages: [],
            jobs: [],
            pipeline: {
                duration: dataset.all.duration_stats,
                started_offset: dataset.all.started_offset_stats,
                queued_duration: dataset.all.started_offset_stats,
                status: dataset.all.status_stats
            }
        };
        for (const merged_job of Object.values(dataset.merged_jobs)) {
            let job_ids: number[] = merged_job.jobs.map((job) => job.id);
            dataset_json.jobs.push({
                name: merged_job.name,
                job_ids: job_ids,
                duration: merged_job.duration_stats,
                started_offset: merged_job.started_offset_stats,
                queued_duration: merged_job.started_offset_stats,
                status: merged_job.status_stats,
                on_critical_path: merged_job.on_critical_path,
            });
        }
        for (const merged_stage of Object.values(dataset.merged_stages)) {
            dataset_json.stages.push({
                name: merged_stage.name,
                duration: merged_stage.duration_stats,
                started_offset: merged_stage.started_offset_stats,
                queued_duration: merged_stage.started_offset_stats,
                status: merged_stage.status_stats
            });
        }
        result.push(dataset_json);
    }

    console.log(JSON.stringify(result, null, 2));
}
