import { ADT, matchI } from "ts-adt";

import { Option, some, none, is_none, of_some } from "./option.js";

import { Context } from "./context.js";

import * as Gitlab_refs from "./gitlab_refs.js";

type filters = {
    sha: Option<string>,
    limit: Option<number>,
    jq_filter: Option<string>
}

export let no_filters: filters = {
    sha: none(),
    limit: none(),
    jq_filter: none()
};

export type dataset_ref = ADT<{
    local: { path: string };
    pipeline: {
        pipeline_ref: Gitlab_refs.pipeline_ref
    };
    branch: {
        branch_ref: Gitlab_refs.branch_ref,
        filters: filters,
    };
    merge_request: {
        merge_request_ref: Gitlab_refs.merge_request_ref,
        filters: filters
    };
}>;

export function tostring(ref: dataset_ref): string {
    return matchI(ref)({
        'local': ({ path }) => {
            return path;
        },
        'pipeline': ({ pipeline_ref }) => {
            return Gitlab_refs.pipeline_ref_tostring(pipeline_ref);
        },
        'branch': ({ branch_ref, filters }) => {
            return Gitlab_refs.branch_ref_tostring(branch_ref);
        },
        'merge_request': ({ merge_request_ref, filters }) => {
            return Gitlab_refs.merge_request_ref_tostring(merge_request_ref);
        }
    })
}

function parse_filters(input: string): [filters, string] {
    let s = input;
    let filters = no_filters;
    let sha_match: RegExpMatchArray;
    if (sha_match = s.match(/^@(\w+)/)) {
        filters.sha = some(sha_match[1]);
        s = s.substr(sha_match[0].length);
    }

    let limit_match: RegExpMatchArray;
    if (limit_match = s.match(/^~(\d+)/)) {
        filters.limit = some(parseInt(limit_match[1], 10));
        s = s.substr(limit_match[0].length);
    }

    let filter_match: RegExpMatchArray;
    if (filter_match = s.match(/^\|(\w+)$/)) {
        filters.jq_filter = some(filter_match[1]);
        s = s.substr(filter_match[0].length);
    }

    return [filters, s];
}

export function parse(input: string, def_ns: string, def_proj: string): Option<dataset_ref> {
    let pipeline_ref: Gitlab_refs.pipeline_ref;
    let branch_ref: Gitlab_refs.branch_ref;
    let merge_request_ref: Gitlab_refs.merge_request_ref;
    let s = input;

    if (s.endsWith('.json')) {
        return some({ _type: 'local', path: s });
    } else {
        let ref: dataset_ref;
        let filters: filters;
        let r: any;
        if (r = Gitlab_refs.parse_pipeline_ref(s, def_ns, def_proj)) {
            [pipeline_ref, s] = r;
            ref = { _type: 'pipeline', pipeline_ref };
        } else if (r = Gitlab_refs.parse_merge_request_ref(s, def_ns, def_proj)) {
            [merge_request_ref, s] = r;
            [filters, s] = parse_filters(s);
            ref = { _type: 'merge_request', merge_request_ref, filters };
        } else if (r = Gitlab_refs.parse_branch_ref(s, def_ns, def_proj)) {
            [branch_ref, s] = r;
            [filters, s] = parse_filters(s);
            ref = { _type: 'branch', branch_ref, filters };
        } else {
            console.log("not sure what to do with", s);
            return none();
        }

        if (s.length != 0) {
            console.log("invalid ref " + input + ", trailing input: " + s);
            return none()
        } else {
            return some(ref);
        }
    }
}


export function parse_exn(ctxt: Context, input: string, def_ns: string, def_proj: string): dataset_ref {
    let pipeline_ref_opt = parse(input, def_ns, def_proj)
    if (is_none(pipeline_ref_opt)) {
        ctxt.error("Could not parse pipeline reference: " + input)
    } else {
        return of_some(pipeline_ref_opt);
    }
}
