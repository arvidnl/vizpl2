import { ADT } from "ts-adt";

export type Option<A> = ADT<{
    some: { value: A };
    none: {};
}>;

export function some<A>(value: A): Option<A> {
    return { _type: 'some', value };
}


export function none<A>(): Option<A> {
    return { _type: 'none' };
}

export function is_some<A>(opt: Option<A>): boolean {
    return opt._type == 'some';
}

export function is_none<A>(opt: Option<A>): boolean {
    return opt._type == 'none';
}

export function of_some<A>(opt: Option<A>): A {
    return opt['value'];
}

export function map<A, B>(opt: Option<A>, fn: (v: A) => B) {
    return is_some(opt) ? some(fn(of_some(opt))) : none();
}

export function fold<A, B>(opt: Option<A>, fn: (v: A) => B, def: B) {
    return is_some(opt) ? fn(of_some(opt)) : def;
}

