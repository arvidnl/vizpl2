import { readFileSync } from 'fs';
import { resolve } from 'path';
import { dirname } from 'path';
import { fileURLToPath } from 'url';

export function version(): string {
    const __dirname = dirname(fileURLToPath(import.meta.url));
    let pkg = JSON.parse(readFileSync(resolve(__dirname, '../package.json'), 'utf8'));
    return pkg.version;
}
