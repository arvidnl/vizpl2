import { Option, is_some, of_some } from "./option.js";

export type formatter = (n: number, _baseline_seconds: Option<number>) => string;

export function percentage_diff(new_value: number, baseline_value: number): number {
    if (new_value == baseline_value) {
        return 0;
    } else if (new_value < baseline_value) {
        return -((baseline_value - new_value) / baseline_value) * 100;
    } else if (new_value > baseline_value) {
        return ((new_value - baseline_value) / baseline_value) * 100;
    }
}

export function percentage_diff_format(new_value: number, baseline_value: number): string {
    let d = percentage_diff(new_value, baseline_value);
    if (d == 0 || isNaN(new_value) || isNaN(baseline_value)) {
        return '';
    } else {
        if (d > 0) {
            return ' (+ %' + d2(d) + ')';
        } else {
            return ' (- %' + Math.abs(d2(d)) + ')';
        }
    }
}

export function d2(seconds: number): number {
    return (Math.round(seconds * 100) / 100);
}

export function d2_formatter(
    seconds: number,
    baseline_seconds: Option<number>
): string {
    let str = "" + d2(seconds);

    if (is_some(baseline_seconds)) {
        str += percentage_diff_format(seconds, of_some(baseline_seconds));
    }

    return str;
}

export function humanize_time(
    seconds: number,
): string {
    if (isNaN(seconds)) {
        return "NaN";
    }

    let str = ""

    if (seconds >= 3600) {
        let hours = Math.floor(seconds / 3600);
        str += Math.floor(seconds / 3600) + "h";
        seconds -= hours * 3600;
    }
    if (seconds >= 60) {
        let minutes = Math.floor(seconds / 60);
        str += Math.floor(seconds / 60) + "m";
        seconds -= minutes * 60;
    }
    str += d2(seconds) + "s";

    return str;
}

export function humanize_formatter(
    seconds: number,
    baseline_seconds: Option<number>
): string {
    let diff_str: string = is_some(baseline_seconds)
        ? percentage_diff_format(seconds, of_some(baseline_seconds))
        : '';

    return humanize_time(seconds) + diff_str;
}
