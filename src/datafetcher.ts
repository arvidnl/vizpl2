import { Context } from "./context.js";
import * as Pipelines from "./pipelines.js";
import * as Gitlab from "./gitlab.js";
// import { MergedJob } from "./pipelines.js";
import { output_type } from "./output.js";

import { matchI } from "ts-adt";

import { Option, some, none, is_none, is_some, of_some } from "./option.js";

import * as formatting from "./formatting.js";

import { pad_right } from './string.js';

import { version } from './version.js';

import * as Dataset_ref from "./dataset_ref.js";

export interface Config {
    formatter: formatting.formatter;
    outputs: Set<output_type>;
    job_filter: Option<RegExp>;
    sort: Pipelines.Sort;
    max_column_width: number;
    merge_stages: boolean;
    color: boolean;
    include_retried: boolean;
}

export interface Arguments {
    config: Config,
    datasets: Pipelines.Dataset[];
}

const default_reference_namespace: string = 'arvidnl';

const default_reference_project: string = 'vizpl2';

/*

  references:
  - pipeline.json = a local file containing the results of a get jobs api call
  - will fetch the local file if it exists
  - ((project)/repo)#pipeline_id (e.g. tezos/tezos#386210384)
  - will fetch the pipeline jobs from the api

  todo:

  - ((namespace)/project)/branch(@commit)
  - fetch pipelines that have run on this branch (and commit)

  - ((namespace)/project)!mr(@commit)
  - fetch merge request pipelines that have run on this mr (and commit)

  - above~N
  - fetch N latest pipelines of branch / MR

  - above|jq-expression
  - above||jmes-expression
  - as above but apply jq expression to filter results

*/

function resolve_dataset_reference(
    ctxt: Context,
    dataset_ref: Dataset_ref.dataset_ref,
    include_retried: boolean
): Promise<Pipelines.raw_pipeline[]> {
    // cast object to Pipelines.raw_pipeline
    let read_local_pipeline: (include_retried: boolean, ref: string) => Promise<Pipelines.raw_pipeline>
        = (_include_retried, ref) => ctxt.read_local_json(ref).then((obj: Pipelines.raw_pipeline) => obj)

    function singleton<T>(x: T): T[] { return [x]; };

    return matchI(dataset_ref)({
        'local': async ({ path }) => {
            return read_local_pipeline(include_retried, path)
                .then(singleton);
        },
        'pipeline': async ({ pipeline_ref }) => {
            return Gitlab
                .project_pipelines_jobs(ctxt, pipeline_ref, include_retried)
                .catch((error) => {
                    console.log('Reference ' + Dataset_ref.tostring(dataset_ref) + ' could not be resolved (error code ' + error.statusCode + '):');
                    console.log(error.body);
                    process.exit(1);
                })
                .then(singleton);
        },
        'branch': ({ branch_ref, filters }) => {
            return Gitlab
                .project_branch_pipelines_jobs(ctxt, branch_ref, filters.sha, filters.limit, include_retried);
        },
        'merge_request': ({ merge_request_ref, filters }) => {
            console.log("mr (TODO)", merge_request_ref, filters);
            return Promise.resolve([]);
        }
    });
}

async function pmap<T1, T2>(p: Promise<T1>, f: (v: T1) => T2): Promise<T2> {
    // return p.then((v : T1) => f(v))
    return p.then(f)
}

type arg_spec = {
    flag: string,
    description: string,
    fun: (args: string[]) => void
}

function parse_args(args_spec: arg_spec[], argv: string[], usage_help: string): void {
    let arg_map = {};
    for (let arg of args_spec) {
        arg_map['--' + arg.flag] = arg;
    }

    while (argv.length) {
        let arg = argv[0];
        if (arg_map.hasOwnProperty(arg)) {
            let arg_spec = arg_map[arg];
            argv.shift();
            arg_spec.fun(argv);
        } else if (arg == "--help") {
            console.log(usage_help);
            let arg_max_width = 0;
            let flag_desc: string[][] = [];
            for (let arg of args_spec) {
                flag_desc.push([arg.flag, arg.description]);
            }
            flag_desc.push(["help", " Show this help."])
            flag_desc.push(["version", " Show version."])
            let flag_arg_desc: string[][] = [];
            for (let [flag, descr] of flag_desc) {
                let m = descr.match(/^([^ ]*)(.*)/);
                let arg_flag_descr = "";
                let description = "";
                if (m) {
                    arg_flag_descr = flag + ' ' + m[1];
                    description = m[2];
                } else {
                    arg_flag_descr = flag;
                    description = descr;
                }
                flag_arg_desc.push([arg_flag_descr, description]);
                arg_max_width = Math.max(arg_max_width, arg_flag_descr.length);
            }
            for (let [flag, descr] of flag_arg_desc) {
                console.log(`  --${pad_right(flag, arg_max_width)} ${descr}`);
            }
            process.exit(1);

        } else if (arg == "--version") {
            console.log(version());
            process.exit(0);
        } else {
            break;
        }
    }
}

export async function parse_command_line(
    ctxt: Context,
    args: string[],
): Promise<Arguments> {
    // parse output configuration
    let config: Config = {
        formatter: (secs, b) => formatting.humanize_formatter(secs, b),
        outputs: new Set(),
        job_filter: none(),
        sort: Pipelines.Sort.Duration,
        max_column_width: Number.isInteger(process.stdout.columns)
            ? process.stdout.columns
            : 80,
        merge_stages: false,
        color: Boolean(process.stdout.isTTY),
        include_retried: true
    };

    let args_spec: arg_spec[] = [
        {
            flag: "humanize",
            description: " Format durations in HH:MM:SS format (default).",
            fun: (_args) => {
                config.formatter = (secs, b) => formatting.humanize_formatter(secs, b);
            }
        },
        {
            flag: "seconds",
            description: " Format durations as seconds.",
            fun: (_args) => {
                config.formatter = (secs, b) => formatting.d2_formatter(secs, b);
            }
        },
        {
            flag: "table",
            description: " Present results as a table.",
            fun: (_args) => {
                config.outputs.add(output_type.Table);
            }
        },
        {
            flag: "graph",
            description: " Present results as a graph.",
            fun: (_args) => {
                config.outputs.add(output_type.Graph);
            }
        },
        {
            flag: "json",
            description: " Present results as json.",
            fun: (_args) => {
                config.outputs.add(output_type.Json);
            }
        },
        {
            flag: "job-filter",
            description: "<REGEXP> Filter job by regexp. Example: '--job-filter \"build_\\*\"'.",
            fun: (args) => {
                if (args.length == 0) {
                    ctxt.error("--job-filter takes exactly one argument");
                }
                if (is_some(config.job_filter)) {
                    ctxt.error("--job-filter can only be given once");

                }
                config.job_filter = some(new RegExp(args[0]));
                args.shift();
            }
        },
        {
            flag: "merge-stages",
            description: " Merge pipelines stages.",
            fun: (_args) => {
                config.merge_stages = true;
            }
        },
        {
            flag: "sort-by-started-at",
            description: " Sort jobs by start time.",
            fun: (_args) => {
                config.sort = Pipelines.Sort.Started_at;
            }
        },
        {
            flag: "sort-by-duration",
            description: " Sort jobs by duration.",
            fun: (_args) => {
                config.sort = Pipelines.Sort.Duration;
            }
        },
        {
            flag: "max-column-width",
            description: "<WIDTH> Maximum column width.",
            fun: (args) => {
                let w: number;
                if (args.length == 0 || Number.isNaN(w = parseInt(args[0], 10))) {
                    ctxt.error("--max-column-width takes exactly one numerical argument");
                }
                config.max_column_width = w;
                args.shift();
            }
        },
        {
            flag: "no-color",
            description: " Disable color in output.",
            fun: (_args) => {
                config.color = false;
            }
        },
        {
            flag: "color",
            description: " Enable color in output.",
            fun: (_args) => {
                config.color = true;
            }
        },
        {
            flag: "no-include-retried",
            description: " Do not include retried jobs.",
            fun: (_args) => {
                config.include_retried = false;
            }
        },
    ];



    let usage_help = `Usage: vizpl2 [OPTIONS] [PIPELINE_REFERENCES]

Visualize the executions of the GitLab CI pipelines PIPELINE_REFERENCES.
Example: vizpl2 --graph arvidnl/vizpl2#725494153

PIPELINE REFERENCES

  Remote pipelines are fetched through GitLab's API and are referred to
  either by a pipeline reference 'NAMESPACE/PROJECT#PIPELINE_ID', or by an
  URL 'https://gitlab.com/NAMESPACE/PROJECT/-/pipelines/PIPELINE_ID'.
  Alternatively, a path to a JSON file containing results of GitLab's
  'List pipeline jobs' REST API can be given 'local_file.json' (see
  https://docs.gitlab.com/ee/api/jobs.html#list-pipeline-jobs).

  Multiple references can be supplied. By default, each reference form
  a single dataset containing a single pipeline, labelled by the reference.

  A sequence of references can be merged to a single dataset when
  terminated by a '--label LABEL' flag.  In this case, if no terminating
  '--label' flag is given, all trailing references are merged into an
  anonymous dataset.

EXAMPLES

  For instance:

    vizpl2 pipeline-123123.json

  visualizes the result in 'pipeline-123123.json'.

    vizpl2 ns/proj#111 ns/proj#222

  gives two datasets:
    - a dataset 'ns/proj#111' containing #111,
    - a dataset 'ns/proj#222' containing #222,

    vizpl2 ns/proj#111 ns/proj#222 --label ds1 ns/proj#333 --label ds2 ns/proj#444

  gives the following datasets:
    - a dataset 'ds1' containing pipelines #111 and #222,
    - a dataset 'ds2' containing pipeline #333,
    - an anonymous dataset containing pipeline #444.


OPTIONS
`;


    parse_args(args_spec, args, usage_help);

    if (args.length == 0) {
        ctxt.error(
            "Must provide data sets as args, see --help. Note that pipeline references\n" +
            "without NAMESPACE/PROJECT qualifiers (e.g. '#123456789') may require quoting."
        );
    }

    config.outputs = config.outputs.size
        ? config.outputs
        : new Set([output_type.Table]);


    // parse data set references
    let datasets: Promise<Pipelines.Dataset>[] = [];
    if (args.indexOf("--label") >= 0) {
        let pipelines: Promise<Pipelines.pipeline[]>[] = [];

        while (args.length > 0) {
            let arg = args.shift();

            if (arg == "--label") {
                if (args.length == 0) {
                    ctxt.error("Must provide label after --label, see --help")
                } else if (pipelines.length == 0) {
                    ctxt.error("No datasets provided before label, see --help")
                } else {
                    let label = args.shift();
                    let ds_ps =
                        Promise.all(pipelines).then((ps) => ({
                            label: label,
                            pipelines: ps.flat()
                        }));
                    datasets.push(ds_ps);
                    pipelines = [];
                }
            } else {
                let ref = Dataset_ref.parse_exn(ctxt, arg, default_reference_namespace, default_reference_project);
                let next = resolve_dataset_reference(ctxt, ref, config.include_retried);
                pipelines.push(next.then(
                    (ps) => ps.map((p) => Pipelines.annotate_pipeline(p, config.merge_stages))
                ));
            }
        }

        if (pipelines.length > 0) {
            let ds_p = Promise.all(pipelines).then((pipelines) => ({
                label: "Unnamed dataset",
                pipelines: pipelines.flat()
            }));
            datasets.push(ds_p);
        }
    } else {
        datasets = args.map(async (arg) => {
            let ref = Dataset_ref.parse_exn(ctxt, arg, default_reference_namespace, default_reference_project);
            return resolve_dataset_reference(ctxt, ref, config.include_retried).then(
                (raw_pipelines: Pipelines.raw_pipeline[]) => {
                    return Promise.resolve({
                        label: Dataset_ref.tostring(ref),
                        pipelines: raw_pipelines.map((p) =>
                            Pipelines.annotate_pipeline(p, config.merge_stages))
                    })
                }
            )
        });
    }

    return Promise.all(datasets).then((datasets) =>
        Promise.resolve({
            config,
            datasets
        })
    );
}
