Do not actually print the version so that we do not have to update
this file in each version.

  $ ./test.sh --version | tr '[0-9]' 'X'
  X.X.X
