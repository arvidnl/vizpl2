# Changelog

## Development Version

### Breaking Changes

 - For jobs with no queued duration, the bar trailer in `--graph` mode
   only shows the duration.

 - `--max-column-width` now defaults to 80 with non-tty outputs.

### New Features

 - Add failure status to bars in when using `--graph` visualization.

 - Results now include retried jobs, which appear as separate jobs
   with a name suffixed `#N` where `N` is the 1-indexed run of the
   job.

 - Add flags `--no-color` (respectively `--color`) that enables
   respectively enables color output. By default, color is enabled
   when the output is a TTY.

 - Add flag `--json` for JSON output.

 - Add flag `--no-include-retried`. If set, hides retried jobs in
   output.

 - The jobs composing the critical path of a pipeline is inferred and
   marked with a `*` in `--graph` output.

 - `--table` output now contains a bottom row with the mean sequential
   time per pipeline in each dataset.

 - Pipelines can be referenced on the command line through their URL
   (e.g. `https://gitlab.com/namespace/project/-/pipelines/<pipeline_id>`).

### Bug Fixes

 - Fix incorrect color cycling in `--graph` for pipelines with more stages
   than colors in the palette.

 - Fix issue where queued duration / duration would appear as `NaN`

 - Fix bug in `--graph` where the first job would be shifted to the right.

 - Fix `--max-column-width` being undefined in non-tty outputs.

 - Fix `RangeError: Invalid count value` issue in `asciigraph.ts` by
   ensuring that resolution is strictly positive.

 - Fix incorrect handling of command-lines lacking pipeline references.

 - Fix percentage diff between two `NaN` values (appeared as `NaN (- %NaN)`)

 - Improved error message for unfound remote references (prettier 404 messages).

 - Fix the inclusion of retried jobs, which was set to false by
   default and could not be set to true.

 - Fix the display of percentage diffs.

 - Fix the duration of pipelines with jobs lacking `started_at`.

## Older versions

vizpl2 0.1.0 to 0.1.3 was released in npm but were not announced.


