import { Context } from "./context.js";
import * as Pipelines from "./pipelines.js";
import { Option, fold, none, some } from "./option.js";

import * as Refs from "./gitlab_refs.js"

export async function project_pipelines_jobs(
    ctxt: Context,
    pipeline_ref: Refs.pipeline_ref,
    include_retried: boolean
): Promise<Pipelines.raw_pipeline> {

    let children = await child_pipelines(ctxt, pipeline_ref);
    let pipelines = children.concat([pipeline_ref]);
    let jobs = await Promise.all(
        pipelines.map(async (pipeline_ref) =>
            loop_project_pipelines_jobs(ctxt, pipeline_ref, include_retried)));
    return jobs.flat();
}

async function loop_project_pipelines_jobs(
    ctxt: Context,
    pipeline_ref: Refs.pipeline_ref,
    include_retried: boolean,
    page: number = 1,
): Promise<Pipelines.raw_pipeline> {
    let project_id_s = encodeURIComponent(Refs.project_id_tostring(pipeline_ref.project_id));
    let url = "https://gitlab.com/api/v4/projects/"
        + project_id_s
        + "/pipelines/"
        + pipeline_ref.id
        + "/jobs?per_page=200"
        + "&page=" + page
        + "&include_retried=" + (include_retried).toString();

    return ctxt.read_remote_json(url).then(
        (jobs1: Pipelines.raw_pipeline) => {
            if (jobs1.length < 100) return jobs1;
            return loop_project_pipelines_jobs(ctxt, pipeline_ref, include_retried, page + 1)
                .then((jobs2) => jobs1.concat(jobs2))
        });

}

async function child_pipelines(
    ctxt: Context,
    pipeline_ref: Refs.pipeline_ref
): Promise<Refs.pipeline_ref[]> {
    let project_id_s = encodeURIComponent(Refs.project_id_tostring(pipeline_ref.project_id));
    let url = "https://gitlab.com/api/v4/projects/"
        + project_id_s
        + "/pipelines/"
        + pipeline_ref.id
        + "/bridges";

    return ctxt.read_remote_json(url).then(
        (bridges: Pipelines.raw_bridges) => {

            return bridges
                .filter((b) =>
                    b.downstream_pipeline && b.downstream_pipeline.id)
                .map((b) => {
                    return {
                        id: b.downstream_pipeline.id,
                        project_id: pipeline_ref.project_id
                    };
                });
        });

}

type gitlab_pipeline = {
    'id': number,
    'iid': number,
    'status': string
}

export async function project_branch_pipelines_jobs(
    ctxt: Context,
    branch_ref: Refs.branch_ref,
    sha: Option<string>,
    limit: Option<number>,
    include_retried: boolean
): Promise<Pipelines.raw_pipeline[]> {
    let project_id_s = encodeURIComponent(Refs.project_id_tostring(branch_ref.project_id));
    let url = "https://gitlab.com/api/v4/projects/"
        + project_id_s
        + "/pipelines?"
        + "ref=" + branch_ref.name
        + fold(sha, (v => "&sha=" + v), '')
        + fold(limit, (v => "&per_page=" + v), '');

    return ctxt.read_remote_json(url).then(
        (json: gitlab_pipeline[]) => {
            return Promise.all(json.map((json: gitlab_pipeline) => {
                let pipeline: Refs.pipeline_ref = {
                    'id': json.id,
                    'project_id': branch_ref.project_id
                };
                return project_pipelines_jobs(ctxt, pipeline, include_retried);
            }));
        }
    );

}

export async function resolve_branch_ref(
    _ctxt: Context,
    _branch_ref: Refs.branch_ref
): Promise<Pipelines.raw_pipeline[]> {
    // Project.
    return Promise.resolve([]);
}
